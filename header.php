<!DOCTYPE html>
<html lang="en">
<head>
  <title>DS Micro site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favi-icon-blk.png" type="image/png" >
  <link rel="stylesheet" href="css/bootstrap.min.css">  
 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.css">
 <link rel="stylesheet" href="css/aos.css">
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/new.css">
  <link rel="stylesheet" href="css/responsive.css">
</head>
<body >
<header class="header" id="home-banner">
    <div class="container50">
        <div class="logo">
          <img class="iconLogoImg iconLogoImgbk img-fluid" src="images/favi-icon-blk.png" />
          <span class="pagetitle" id="titleB">Branding</span>
          <span class="pagetitle" id="titleDE">Digital  Experience</span>
          <span class="pagetitle" id="titleDA">Digital  Activation</span>
          <span class="pagetitle" id="titleWWA">Who we are</span>
          <span class="pagetitle" id="titleG">Goodies</span>
          <span class="pagetitle" id="titleGIT">Get in touch</span>
        </div>
        <div class="logo whitelogo">
        <img class="logImg img-fluid" src="images/logo-white.png" /> 
          <img class="iconLogoImg img-fluid" src="images/favi-icon-white.png" /> 
        </div>
        <div class="menuIcon hover-target menuIconOpen" >
          <div class="bar1"></div>
          <div class="bar2"></div>
          Menu
        </div>
        <div class="menuIcon menuIconBlk hover-target menuIconOpen" >
            <div class="bar1"></div>
            <div class="bar2"></div>
        </div>
    </div>
  </header>
  <div class="sidebar">
    <div class="menuIcon menuIconClose hover-target" >
        <div class="bar1"></div>
        <div class="bar2"></div>
        Close
    </div>
    <nav id="menu"> 
      <ul class="parent-menu hover-target "> 
        <li>
          <a  href="#1" >What we do <span>We do good</span></a>
        </li>
        <li>
          <a  href="#2">Branding<span>Identity Creation/ Web/ Content Strategy/SEO and more</span></a>
        </li>
        <li>
          <a  href="#3">Digital Experience <span>Campaign Strategy/ Interactive Marketing/ Social Media and more</span></a>          
        </li>
        </li>
        <li><a data-index="4" href="#4">Digital Activation <span>Ecommerce platforms/ AR/VR/ Gamification/ Hybrid analytics and more</span></a></li>
        <li><a data-index="5" href="#5">Who we are <span>The good folks</span></a></li>
        <li><a data-index="6" href="#6">Goodies <span>Some extra input</span></a></li>
        <li><a data-index="7" href="#7">Get in touch <span>With a cup of coffee</span></a></li>
        
      </ul> 
    </nav>
    <div class="sideFooter">
      <ul class="sideSocial">
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/facebook-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/linkedin-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/twitter-g.png"></a></li>
      </ul>
      <a class="notdisplaymb hover-target" href="mailto:info@knowledgesynonyms.com">info@knowledgesynonyms.com</a>
    </div>
  </div>

