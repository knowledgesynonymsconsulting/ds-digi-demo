<!DOCTYPE html>
<html lang="en">
<head>
  <title>DS Micro site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favi-icon-blk.png" type="image/png" >
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/aos.css" />
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/new.css">
  <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="">
  <header class="header headerBk">
    <div class="container50">
        <div class="logo">
            <!-- <img class="logImg img-fluid" src="images/logo-blk.png" />  -->
            <a href="http://demo.knowledgesynonyms.com/ds-v2/"><img class="iconLogoImg img-fluid" src="images/favi-icon-blk.png" /> </a>
            <span class="pagetitle pageinnertitle">Branding</span>
        </div>
        
        <div class="menuIcon menuIconBlk menuIconOpen" >
            <div class="bar1"></div>
            <div class="bar2"></div>
            <!-- Menu -->
        </div>
        <div class="closeIcon hover-target">
          <a href="http://demo.knowledgesynonyms.com/ds-v2/#2">
            <img class="img-fluid" src="images/close-icon.png" />
          </a>
        </div>
    </div>
  </header>
  <div class="sidebar">
    <div class="menuIcon menuIconClose hover-target" >
        <div class="bar1"></div>
        <div class="bar2"></div>
        Close
    </div>
    <nav id="menu"> 
      <ul class="parent-menu hover-target "> 
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#1">What we do <span>We do good</span></a>
        </li>
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#2"  >Branding<span>Identity Creation/ Web/ Content Strategy/ SEO and more</span></a>
        </li>
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#3">Digital Experience <span>Campaign Strategy/ Interactive Marketing/ Social Media and more</span></a>          
        </li>
        </li>
        <li><a data-index="4" href="http://demo.knowledgesynonyms.com/ds-v2/#4">Digital Activation <span>Ecommerce platforms/ AR/VR/ Gamification/ Hybrid analytics and more</span></a></li>
        <li><a data-index="5" href="http://demo.knowledgesynonyms.com/ds-v2/#5">Who we are <span>The good folks</span></a></li>
        <li><a data-index="6" href="http://demo.knowledgesynonyms.com/ds-v2/#6">Goodies <span>Some extra input</span></a></li>
        <li><a data-index="7" href="http://demo.knowledgesynonyms.com/ds-v2/#7">Get in touch <span>With a cup of coffee</span></a></li>
        
      </ul> 
    </nav>
    <div class="sideFooter">
      <ul class="sideSocial">
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/facebook-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/linkedin-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/twitter-g.png"></a></li>
      </ul>
      <a class="notdisplaymb hover-target" href="mailto:info@knowledgesynonyms.com">info@knowledgesynonyms.com</a>
    </div>
  </div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" rel="stylesheet"/>


<div class="wrapper-main ">
  <div class="wrapper-inner">
      <div class="barndingsection">
          <div class="container50">
              <div class="about1 flexContainer heightVH">
                  <div class="flex60 slideInLeft in-left" >
                      <img class="img-fluid desktop" src="images/eye.jpg">
                      <img class="img-fluid ipad-lanscap " src="images/DS_ipad_530x6108.jpg" >
                  </div>            
                  <div class="flex40 textBoxRight in-right">
                      <div class="textinnerbox">
                        <h1 class="textColored hover-target text-what-top"><span class="Display-MB">Branding</span>We will open your eyes</h1>
                        <div class="textBoxRightInner">
                          <h4 class="subheadeing">01 Design/ Identity Creation</h4>
                          <p >To be successful, you need to be what your customers want you to be. We help you create that unique identity that becomes synonymous with your company. Such a definition helps identify your product/services and distinguish it from the others. A brand identity remind your customers of you.</p> 
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="about2">
          <div class="container50">
              <div class="row">
                  <div class="flex60 text-250-left in-left">
                    <div class="textinnerbox">
                          <h4 class="subheadeing">02 Web/ Mobile</h4>
                          <p>Mobile apps have become a business basic if you want to reach out to your customers and expand beyond political and geographical boundaries. From e-commerce stores and payment banks to food delivery and education, mobile applications continue to prove their marketing value across industries.But before you jump the mobile app bandwagon for your business, you would need to figure out a few things like the desired outcome- Do you want to become an online business or use the app only for queries? Do you want native or web-based, JQuery, JavaScript, CSS, Python, or HTML5 native scrolling, Notepad++, Bootstrap or Angular? Yes, the mumbo jumbo you don’t really need to know because we’ll help you weed out the unnecessary and use suitable goals to create an app/website where your brand identity should come through in full force. </p>
                      </div>
                  </div>
                  <div class="flex40 text-250-right in-right">                    
                      <img class="img-fluid web-img " src="images/web-mobile.jpg">
                  </div>
              </div>               
              <div class="row">                                       
                  <div class="flex60">
                  </div>
                  <div class="flex40 text-250-right in-right">
                      <div class="text-250-right" >
                          <div class="textinnerbox">
                            <h4 class="subheadeing">03 Content Strategy, Research, SME </h4>
                              <p>A successful content marketing generates higher sales. An intelligent marketing campaign leverages different tactics like SEO, long-form content, infographics, etc. to compel them towards a purchase. And that is EXACTLY what we do. </p>
                          </div>
                      </div>
                  </div> 
              </div>
              <div class="row"> 
                  <div class="flex60 text-250-left in-left">
                      <div class="textinnerbox">
                          <h4 class="subheadeing">04 SEO</h4>
                          <p>Next step. Let’s get you on the map (Socially/metaphorically). Let’s crack the code and ensure your customers can find you easily on a search engine’s results page. We help you identify the correct keywords to make your website to appear in every relevant search. </p> 
                      </div>                       
                    </div>
              <div class="col-lg-12">
                  <div class="nextTopic hover-target"><a href="http://demo.knowledgesynonyms.com/ds-v2/#3">Digital Experience <img class="img-fluid arrow-blink" src="images/next-icon.png"></a></div>
              </div>
          </div>
      </div>
    </div>
</div>
<?php include 'footer.php' ?>
