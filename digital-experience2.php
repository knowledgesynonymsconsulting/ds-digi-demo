<!DOCTYPE html>
<html lang="en">
<head>
  <title>DS Micro site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favi-icon-blk.png" type="image/png" >
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/aos.css" />
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/new.css">
  <link rel="stylesheet" href="css/responsive.css">
</head>
<body class="flipInX">
  <header class="header headerBk">
    <div class="container50">
        <div class="logo">
            <!-- <img class="logImg img-fluid" src="images/logo-blk.png" />  -->
            <a href="http://demo.knowledgesynonyms.com/ds-v2/"><img class="iconLogoImg img-fluid" src="images/favi-icon-blk.png" /> </a>
            <span class="pagetitle pageinnertitle">Digital  Experience</span>
        </div>
        <div class="menuIcon menuIconBlk menuIconOpen">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <!-- Menu -->
        </div>
        <div class="closeIcon hover-target">
          <a href="http://demo.knowledgesynonyms.com/ds-v2/#3">
            <img class="img-fluid" src="images/close-icon.png" />
          </a>
        </div>
    </div>
  </header>
  <div class="sidebar">
    <div class="menuIcon menuIconClose hover-target" >
        <div class="bar1"></div>
        <div class="bar2"></div>
        Close
    </div>
    <nav id="menu"> 
      <ul class="parent-menu hover-target "> 
      <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#1">What we do <span>We do good</span></a>
        </li>
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#2"  >Branding<span>Identity Creation/ Web/ Content Strategy/ SEO and more</span></a>
        </li>
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#3">Digital Experience <span>Campaign Strategy/ Interactive Marketing/ Social Media and more</span></a>          
        </li>
        </li>
        <li><a data-index="4" href="http://demo.knowledgesynonyms.com/ds-v2/#4">Digital Activation <span>Ecommerce platforms/ AR/VR/ Gamification/ Hybrid analytics and more</span></a></li>
        <li><a data-index="5" href="http://demo.knowledgesynonyms.com/ds-v2/#5">Who we are <span>The good folks</span></a></li>
        <li><a data-index="6" href="http://demo.knowledgesynonyms.com/ds-v2/#6">Goodies <span>Some extra input</span></a></li>
        <li><a data-index="7" href="http://demo.knowledgesynonyms.com/ds-v2/#7">Get in touch <span>With a cup of coffee</span></a></li>
        
      </ul> 
    </nav>
    <div class="sideFooter">
      <ul class="sideSocial">
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/facebook-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/linkedin-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/twitter-g.png"></a></li>
      </ul>
      <a class="notdisplaymb hover-target" href="mailto:info@knowledgesynonyms.com">info@knowledgesynonyms.com</a>
    </div>
  </div>

  <main>
		<div data-scroll class="page">
      <div class="wrapper-main " >
          <div class="container50">
              <div class="experiencesection" id="campaignStrategy">
                  <div class="about1 flexContainer heightVH">
                      <div class="flex60 tabletDisplay MB">
                          <img class="img-fluid" src="images/spaghetti.jpg" data-aos="fade-left" data-aos-duration="3000" data-aos-easing="ease-in-out">
                      </div>
                      <div class="flex40 about1Left textBoxLeft in-left">
                          <div class="textinnerbox">
                                <h1 class="textColored hover-target text-what-top"><span class="Display-MB">Digital Experience</span>What we do, we do good!</h1>
                                <div class="textBoxLeftInner">
                                <h4 class="subheadeing" >01 Campaign Strategy, design and production</h4>
                                <p >We know that first impressions matter Viral videos, awesome imagery, cool graphics all catered to targeting people who matter to your business.</p>                
                                <!-- <div class="bounceBox"><a href=""><span></span></a></div> -->
                              </div>
                          </div>
                      </div>
                      <div class="flex60 tabletNotDisplay in-right">
                          <img class="img-fluid desktop" src="images/spaghetti.jpg">
                          <img class="img-fluid ipad-lanscap" src="images/DS_ipad_530x6106.jpg">
                      </div>
                  </div>
              </div>
              <div class="content content--full content--alternate">
                <div class="about2">
                        <div class="row" id="interactiveMarketing">
                            <div class="flex40">                        
                            </div>
                            <div class="flex60 text-250-right in-right">
                            <div class="textinnerbox">
                                <h4 class="subheadeing">02 Interactive marketing</h4>
                                <p data-aos="fade-up">Get your audience engaged. If they are investing their time, they’ll likely stick with you!</p>
                            </div>
                            </div>
                        </div>
                        <div class="row" id="richmedia">
                            <div class="flex40 text-250-left in-left">
                              <div class="textinnerbox">
                                      <h1 class="Up-down-heading">Up and down, <br/>right and left</h1>
                                      <div class="leftside-text">
                                      <h4 class="subheadeing">03 Responsive, rich media and multimedia design</h4>
                                      <p data-aos="fade-up">Design is like a painting, it never dies. It’s the mark of your company. “Create ads that tie you to your audience”.</p>
                                  </div>
                                </div>
                            </div>
                            <div class="flex60 text-250-right in-right">
                              <div class="content__item ">	
                                <div class="imgbox" ><img class="img-fluid about2img content__item-img" src="images/about2.jpg">></div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="flex40"></div>
                            <div class="flex60 text-250-right in-right">
                              <div class="textinnerbox">
                                  <h4 class="subheadeing">04 Social Offline<br> (social butterfly)</h4>
                                  <p>Meet your audience where they are - Social Media. Our Social Butterflies will help you identify your audience and market yourself in the best way possible.</p>
                              </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="nextTopic hover-target" data-aos="zoom-in-up" data-aos-duration="2000"><a href="http://demo.knowledgesynonyms.com/ds-v2/#4">Digital Activation <img class="img-fluid arrow-blink" src="images/next-icon.png"></a></div>
                        </div>  
                </div>
              </div>
          </div>
      </div>
    </div>
</main>


<?php include 'footer.php' ?>





