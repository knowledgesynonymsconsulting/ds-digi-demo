<!DOCTYPE html>
<html lang="en">
<head>
  <title>DS Micro site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favi-icon-blk.png" type="image/png" >
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/new.css">
  <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
  <header class="header headerBk">
    <div class="container50">
        <div class="logo">
            <!-- <img class="logImg img-fluid" src="images/logo-blk.png" />  -->
            <a href="http://demo.knowledgesynonyms.com/ds-microsite/"><img class="iconLogoImg img-fluid" src="images/favi-icon-blk.png" /> </a>
        </div>
        <div class="menuIcon menuIconBlk hover-target" onclick="myFunction(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            Menu
        </div>
        <div class="closeIcon hover-target">
          <a href="http://demo.knowledgesynonyms.com/ds-microsite/">
            <img class="img-fluid" src="images/close-icon.png" />
          </a>
        </div>
    </div>
  </header>
  <div class="sidebar">
    <nav id="menu"> 
      <ul class="parent-menu hover-target"> 
        <li>
          <a href="http://demo.knowledgesynonyms.com/ds-microsite/#home-banner">What we do</a>
        </li>
        <li>
          <a href="http://demo.knowledgesynonyms.com/ds-microsite/#brandingSectionHome">Branding </a>
          
        </li>
        <li>
          <a href="http://demo.knowledgesynonyms.com/ds-microsite/#digitalEXpSectionHome">Digital Experience</a>
          <ul> 
            <li><a href="#">01 Campaign Strategy, Design and Production</a></li> 
            <li><a href="#">02 Interactive Marketing </a></li> 
            <li><a href="#">03 Responsive, rich Media and Multimedia Design</a></li> 
            <li><a href="#">04 Social Media </a></li> 
          </ul>
        </li>
        <li><a href="http://demo.knowledgesynonyms.com/ds-microsite/#digitalACTSectionHome">Digital Activation</a></li>
        <li><a href="http://demo.knowledgesynonyms.com/ds-microsite/#whoWeAreHome">Who we are</a></li>
        <li><a href="http://demo.knowledgesynonyms.com/ds-microsite/#Goodies">Goodies</a></li>
        <li><a href="http://demo.knowledgesynonyms.com/ds-microsite/#getInTouchHome">Get in touch</a></li>
        
      </ul> 
    </nav>
  </div>
  
  