
<?php include 'header.php' ?>
<div class="mainWrapper">
    
    <section class="home-banner section" id="home-banner">
        <div class="textHello">
            <div class="item1">
                Hello
            </div>
            <div class="item2">
                Konnichiwa
            </div>
            <div class="item3"> 
                Bonjour
            </div>
            <a href="#2" class="moreLinkWt tabletNotDisplay"><div class="moreRead bounceBox bounceBoxWT"><span></span></div></a>
        </div>
        <a href="#2" class="moreLinkWt tabletDisplay"><div class="moreRead bounceBox bounceBoxWT"><span></span></div></a>
        <div class="aboutInner">
            <div class="aboutText">
                <h2>We are the jelly <br>
                    <!-- <span class="pinkText">(in your peanut butter sandwich)</span> -->
                     that makes everything  stick together</h2>
                <div class="aboutText2">
                    <p>We create different, easy to understand and simple brand identities. May it be online or offline, your brand is in good hands. Your customer will thank you.</p>
                    <div class="flex-buttons">
                        <a href="#2" class="button">Branding</a>
                        <a href="#3" class="button">Digital  Experience</a>
                        <a href="#4" class="button">Digital  Activation</a>
                        <div class="imgs"><div class="progress"></div></div>
                    </div>
                </div>
                <div class="aboutText2mb">
                    <p>We create different, easy to understand and simple brand identities. May it be online or offline, your brand is in good hands. Your customer will thank you.</p>
                    <div class="flex-buttonsmb">
                        <div class="buttonmb"><span class="head-text">Branding </span><span class="detail-t">Identity Creation/ Web/ SEO and more <a href="#brandingSectionHome" class="hover-target"><img class="img-fluid" src="images/next-icon-W.png"></span></a></div>
                        <div class="buttonmb"><span class="head-text">Digital  Experience </span><span class="detail-t">Campaign Strategy/ Social Media and more</span> <a href="#digitalEXpSectionHome" class="hover-target"><img class="img-fluid" src="images/next-icon-W.png"></a></div>
                        <div class="buttonmb"><span class="head-text">Digital  Activation </span><span class="detail-t">Ecommerce platforms/ AR/VR/ Gamification and more</span> <a href="#digitalACTSectionHome" class="hover-target"><img class="img-fluid" src="images/next-icon-W.png"></a></div>
                    </div>
                </div>
            </div>
            <!-- <a href="#brandingSectionHome" class="moreLink tabletDisplay"  ><div class="moreRead bounceBox "><span></span></div></a> -->
            <a href="#2" class="moreLinkWt tabletNotDisplay hover-target"><div class="moreRead bounceBox bounceBoxWT"><span></span></div></a>
        </div>
        <!-- <div class="moreRead"><img class="img-fluid" src="images/more-icon.png"></div> -->
    </section>
    <section class="brandingSectionHome section" id="brandingSectionHome">
        <div class="container50">
            <div class="flexContainer">
                <div class="flex40 textBoxLeft">					
                    <h1 class="textColored "><span class="sectionTitle">Branding</span>We will work <br> till the owls<br>  return home</h1>
                    <div class="textBoxLeftInner">
                        <p >Defining your brand is more or less like a journey of the business’s self-discovery. It could be difficult, time consuming, and uncomfortable… but don’t worry, help is on the way. We help you along each step of this journey and stand out from the crowd.</p>
                        <div class="moreLink"  >
                            <a href="branding.php" class="hover-target">
                                <img class="img-fluid tabletNotDisplay" src="images/next-icon.png"><img class="img-fluid tabletDisplay" src="images/next-icon-W.png">
                            </a>
                        </div>
                        <!-- <div class="moreRead"><img class="img-fluid" src="images/more-icon.png"></div> -->
                        <a href="#3" class="moreLink tabletNotDisplay"  ><div class="moreRead bounceBox "><span></span></div></a>
                    </div>
                </div>
                <div class="flex60 whiteImg"><div></div>
                    <img class="img-fluid" src="images/branding.jpg"/>
                </div>
                <a href="#3" class="moreLink MBDownup tabletDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
            </div>
        </div>       
    </section>

    <section class="digitalEXpSectionHome section" id="digitalEXpSectionHome">
        <div class="container50">
            <div class="flexContainer">
                <div class="flex60 whiteImg"><div></div>
                      <img class="img-fluid" src="images/dog.jpg" />
                </div>
                <div class="flex40 textBoxRight">
                    <h1 class="textColored"><span class="sectionTitle">Digital Experience</span>We make you<br> look good, and we <br>mean really good
                    <!-- <span class="pinkText">really good</span> -->
                </h1>
                    <div class="textBoxRightInner">
                        <p>Our Digital Experience tool belt includes Campaign Strategy Design and Production, Interactive Marketing, Multimedia Design, Advertising, Lead Generation, Content Marketing and finally all the other fancy things you need.</p>
                        <div class="moreLink"  ><a href="digital-experience.php" class="hover-target"><img class="img-fluid" src="images/next-icon.png"></a></div>
                        <!-- <div class="moreRead"><img class="img-fluid" src="images/more-icon.png"></div> -->
                        <a href="#4" class="moreLink MBDownup tabletNotDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
                    </div>
                </div>
                <a href="#4" class="moreLink MBDownup tabletDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
            </div>
        </div>        
    </section>
    
    <section class="digitalACTSectionHome section" id="digitalACTSectionHome">
        <div class="container50">
            <div class="flexContainer">
                <div class="flex40 textBoxLeft ">
                    <h1 class="textColored"   ><span class="sectionTitle">Digital Activation</span>Bonjour from<br> the other<br> side</h1>
                    <div class="textBoxLeftInner">                        
                        <p   >Now that your Digital Marketing plans, setting a strategy, content marketing and designing the campaign are done let’s step into the future, shall we?</p>
                        <p   >To set you apart from the mundane, we bring to you…</p>
                        <div class="moreLink"  ><a href="digital-activation.php" class="hover-target"><img class="img-fluid tabletNotDisplay" src="images/next-icon.png"><img class="img-fluid tabletDisplay" src="images/next-icon-W.png"></a></div>
                        <a href="#5" class="moreLink tabletNotDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
                    </div>                  
                </div>
                <div class="flex60 whiteImg"><div></div>
                    <img class="img-fluid" src="images/digital_activation1.jpg"/>
                </div>
                <a href="#5" class="moreLink MBDownup tabletDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
            </div>
        </div>
    </section>

    <section class="whoWeAreHome section white-MB" id="whoWeAreHome">
        <div class="container50">
            <div class="flexContainer">
                <div class="flex40 textBoxLeft">                       
                    <h1 class="textColored"   ><span class="sectionTitle">Who we are</span>Meet the <br><span class="pinkText">good folks</span></h1>
                    <div class="textBoxLeftInner">
                        <p   >We are the usual suspects, somewhere between a family, a grumpy neighbour or easily said a ridiculous outstanding team. We love each team member and the sparkling ideas they bring along. To keep a balanced diet we tend to grab a coffee, a beer or two. We love to chat but even more to listen.</p>
                        <!-- <div class="moreLink"  ><a href="" class="hover-target"><img class="img-fluid" src="images/next-icon.png"></a></div> -->
                        <a href="#6" class="moreLink tabletNotDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
                    </div>               
                </div>
                <div class="flex60 whoWeAreHomeBG">
                    <div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true }'>
                        <div class="gallery-cell">
                            <div class="vidBox" id="box">
                                <video class="vid"  controls="true" id="video3" poster="images/video-1.png">
                                    <source src="images/dummy-video.mp4" type="video/mp4">
                                </video>
                                <div id="other3" class="hide textVideoBox">
                                    This appears after the video ends
                                </div>
                            </div>
                        </div>
                        <div class="gallery-cell">
                            <div class="vidBox" id="box">
                                <video class="vid" controls="true" preload="metadata"  id="video2" poster="images/video-2.png">
                                    <source src="images/dummy-video.mp4" type="video/mp4">
                                </video>
                                <div id="other2" class="hide2 textVideoBox">
                                    This appears after the video ends hhghghghghy
                                </div>
                            </div>
                        </div>
                        <div class="gallery-cell">
                            <div class="vidBox" id="box">
                                <video class="vid" controls="true" preload="metadata"  id="video1" poster="images/video-3.png">
                                    <source src="images/dummy-video.mp4" type="video/mp4">
                                </video>
                                <div id="other1" class="hide1 textVideoBox">
                                    This appears after the video ends
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="#6" class="moreLink MBDownup tabletDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
            </div>
        </div>
    </section>

    <section class="Goodies section" id="Goodies">
        <div class="container50">
            <div class="flexContainer">
                <div class="flex40 textBoxLeft">
                    <h1 class="textColored"   ><span class="sectionTitle">Goodies</span>We needed  this page for  our meta tag.</h1>
                    <div class="textBoxLeftInner"> 
                        <p   >Modio. Mo maiosam, nos doluptat.
                        Aximpeles sernatia sinctatem ut
                        mossi ditia desto molupta ssimi,
                        coreict orporem vel magni consequam, teceped ma is uteniet perio
                        inulparchic tem.</p>
                        <div class="moreLink"  ><a href="" class="hover-target"><img class="img-fluid tabletNotDisplay" src="images/next-icon.png"><img class="img-fluid tabletDisplay" src="images/next-icon-W.png"></a></div>
                        <a href="#7" class="moreLink tabletNotDisplay hover-target"  ><div class="moreRead bounceBox"><span></span></div></a>
                    </div>            
                </div>
                <div class="flex60 whiteImg"><div></div>                    
                    <img class="img-fluid" src="images/goodies.jpg" />
                </div>
                <a href="#7" class="moreLink MBDownup tabletDisplay hover-target"  ><div class="moreRead bounceBox "><span></span></div></a>
            </div>
        </div>
    </section>
    <section class="getInTouchHome section" id="getInTouchHome">
        <div class="container50">
            <div class="flexContainer">
                <div class="flex60 whiteImg"><div></div>  
                <img class="img-fluid" src="images/contact.jpg"/"> 
                <ul class="hover-target footerMenu tabletNotDisplay">
                    <li>Copyright © 2019 DS Digi</li>
                    <li><a href="" class="hover-target">Terms</a></li>
                    <li><a href="" class="hover-target">Privacy Policy</a></li>
                    <li><a href="" class="hover-target">Cookie Policy</a></li>
                </ul>
                </div>
                <div class="flex40 textBoxRight">
                    <h1 class="textColored"   ><span class="sectionTitle">Get in touch</span>Never boring.<br> We have coffee!</h1>
                    <div class="gallery js-flickity" data-flickity-options='{ "freeScroll": true }'>
                        <div class="gallery-cell">
                            <div class="addressBox">
                                <h4>Noida</h4>
                                <p>Digtal Skunks</p>
                                <p>209-210, A-4, Logix Technova</p>
                                <p>Sector-132, Noida 201305, India</p> 
                                <p>T: +91 (0)120 498 4492</p>
                            </div>
                        </div>
                        <div class="gallery-cell">
                        <div class="addressBox">
                                <h4>Cologne</h4>
                                <p>C/O e-Navik GmbH</p>
                                <p>50825 Cologne, Germany</p> 
                                <p>T: + 49 - (0)221 - 79 00 66 45</p>
                            </div>
                        </div>
                        <div class="gallery-cell">
                        <div class="addressBox">
                                <h4>Arvada</h4>
                                <p>14715 W 64th Avenue Unit G</p>
                                <p>Arvada, CO 80004</p> 
                                <p>T: +1 970 444 55005</p>
                            </div>
                        </div>
                    </div>

                    <div class="socialBox">
                        <p class="pinkText">info@knowledgesynonyms.com</p>
                        <ul>
                            <li><a href="" class="hover-target"><img class="img-fluid" src="images/facebook.png"></a></li>
                            <li><a href="" class="hover-target"><img class="img-fluid" src="images/linkedin.png"></a></li>
                            <li><a href="" class="hover-target"><img class="img-fluid" src="images/twitter.png"></a></li>
                        </ul>
                    </div>
                    <ul class="hover-target tabletDisplay">
                        <li class="tabletNotDisplay">Copyright © 2019 DS Digi</li>
                        <li><a href="" class="hover-target">Terms</a></li>
                        <li><a href="" class="hover-target">Privacy Policy</a></li>
                        <li><a href="" class="hover-target">Cookie Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>        
    </section>
	

</div>
<?php include 'footer.php' ?>







