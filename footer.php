<div class="cursor"  id="cursor"></div>
<div class="cursor2"  id="cursor2"></div>
<div class="cursor3"  id="cursor3"></div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.extensions.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.pkgd.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/demo.js"></script>
<script src="js/aos.js"></script>
<script src="js/script.js"></script>
<script>
   window.location.href.split('#')[0];
   $('.mainWrapper').fullpage({
      autoScrolling:true,
	   scrollHorizontally: true,
      scrollBar: true,
   });
   document.getElementsByTagName('body')[0].addEventListener('mousemove', function(n){
      t.style.left = n.clientX + 'px',
      t.style.top = n.clientY + 'px',
      e.style.left = n.clientX + 'px',
      e.style.top = n.clientY + 'px',
      i.style.left = n.clientX + 'px',
      i.style.top = n.clientY + 'px'
   });
   var  t = document.getElementById('cursor'),
        e = document.getElementById('cursor2'),
        i = document.getElementById('cursor3');

   function n(t){
      e.classList.add('hover'), i.classList.add('hover');
   }
   function s(t){
      e.classList.remove('hover'), i.classList.remove('hover');
   }
   s();
   for(var r = document.querySelectorAll('.hover-target'),
   a = r.length -1; a >=0; a--){
      o(r[a])
   }
   function o(t){
      t.addEventListener('mouseover', n), 
      t.addEventListener('mouseout', s);
   }
</script>
</body>
</html>
