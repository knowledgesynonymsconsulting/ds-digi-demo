

$(document).ready(function(){
  $('.iconLogoImg').click(function() {      // When arrow is clicked
        $('body,html').animate({
           scrollTop : 0                       // Scroll to top of body
        }, 500);
  }); 
  $('.menuIconOpen').click(function() {
    $('.sidebar').addClass("test");
    $('body').addClass("fixedScreen");
});


 $('.menuIconClose').click(function(){
  $('.sidebar').removeClass("test");
  $('body').removeClass("fixedScreen");
});

 $('#menu li a').click(function(){
    $('#menu li a').removeClass("active");
    $(this).addClass("active");
    $('.sidebar').toggleClass("test");
 });

  $('#menu li a').click(function(){
     $('#menu li a').removeClass("active");
     $(this).addClass("active");
     $('.sidebar').toggleClass("test");
  });
  jQuery('.bounceBox').click(function(){
     jQuery('.brandingleft').fadeOut();
  });

});

window.onload = function(){
  setTimeout(function(){
     $('.item1').slideUp();
     $('.item2').slideDown();
  },700);
  setTimeout(function(){
     $('.item2').slideUp();
     $('.item3').slideDown();
  }, 1400);
  setTimeout(function(){
     $('.item3').slideUp();
     $('.textHello').slideUp();
     $('.aboutInner').slideDown();
  }, 2100);
 
};

var initialTopOffset = $('.header').offset().top;
  $(window).scroll(function() {
 $(".section").each(function(){   
  var test= $('.section.active').attr('id');
  $('.header').attr('id', test)
});
});

AOS.init();


$(function() {
  $('.MBDownup').on('click', function(e) {
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});


(function($) {
  $.fn.visible = function(partial) {
   
     var $t            = $(this),
        $w            = $(window),
        viewTop       = $w.scrollTop(),
        viewBottom    = viewTop + $w.height(),
        _top          = $t.offset().top,
        _bottom       = _top + $t.height(),
        compareTop    = partial === true ? _bottom : _top,
        compareBottom = partial === true ? _top : _bottom;
   
   return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
   
})(jQuery);

$(window).scroll(function(event) {
  
  $(".whiteImg").each(function(i, el) {
   var el = $(el);
   if (el.visible(true)) {
     el.addClass("fadeIn"); 
   } else {
     el.removeClass("fadeIn");
   }
  });
  
  $(".textColored").each(function(i, el) {
   var el = $(el);
   if (el.visible(true)) {
     el.addClass("textAnimation"); 
     el.removeClass("textAnimation1");
   } else {
     el.addClass("textAnimation1"); 
     el.removeClass("textAnimation");
   }
  });

  $(".textBoxRight").each(function(i, el) {
   var el = $(el);
   if (el.visible(true)) {
     el.addClass("in-right"); 
   } else {
     el.removeClass("in-right");
   }
  });
  
});
/***************************************inner page scroll***********************************************/  

var html = document.documentElement;
var body = document.body;

var scroller = {
 target: document.querySelector(".wrapper-main"),
 ease: 0.05, // <= scroll speed
 endY: 0,
 y: 0,
 resizeRequest: 1,
 scrollRequest: 0,
};

var requestId = null;

TweenLite.set(scroller.target, {
 rotation: 0.01,
 force3D: true
});

window.addEventListener("load", onLoad);

function onLoad() {    
 updateScroller();  
 window.focus();
 window.addEventListener("resize", onResize);
 document.addEventListener("scroll", onScroll); 
}

function updateScroller() {
 
 var resized = scroller.resizeRequest > 0;
   
 if (resized) {    
//   var height = scroller.target.clientHeight;
   body.style.height = "100vh" ;
   scroller.resizeRequest = 0;
 }
     
 var scrollY = window.pageYOffset || html.scrollTop || body.scrollTop || 0;
 scroller.endY = scrollY;
 scroller.y += (scrollY - scroller.y) * scroller.ease;
 if (Math.abs(scrollY - scroller.y) < 0.05 || resized) {
   scroller.y = scrollY;
   scroller.scrollRequest = 0;
 }
 
 TweenLite.set(scroller.target, { 
   y: -scroller.y 
 });
 
 requestId = scroller.scrollRequest > 0 ? requestAnimationFrame(updateScroller) : null;
}

function onScroll() {
 scroller.scrollRequest++;
 if (!requestId) {
   requestId = requestAnimationFrame(updateScroller);
 }
}

function onResize() {
 scroller.resizeRequest++;
 if (!requestId) {
   requestId = requestAnimationFrame(updateScroller);
 }
}


/************************************end inner page scrolling************************************/ 


/**********************************inner page image scrolling*******************************************/ 

// TweenLite.defaultEase = Linear.easeNone;
// var ctrl = new ScrollMagic.Controller();

// // Create scenes
// $(".about2").each(function(i) {
//   let target2 = $(this).find("img");
//   var tl = new TimelineMax();
//   tl.from(target2, 1, { scale: 0.5 }, 0);
//   new ScrollMagic.Scene({
//     triggerElement: this,
//     duration: "50%",
//     triggerHook: 0.5
//   })
//     .setTween(tl)
//     .addTo(ctrl)
//     // .addIndicators({
//     //   colorTrigger: "white",
//     //   colorStart: "white",
//     //   colorEnd: "white",
//     //   indent: 40
//     // });
// });



/**********************************end inner page image scrolling*******************************************/ 
