
<!DOCTYPE html>
<html lang="en">
<head>
  <title>DS Micro site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favi-icon-blk.png" type="image/png" >
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/new.css">
  <link rel="stylesheet" href="css/responsive.css">
<script type="text/javascript" src="http://gc.kis.v2.scr.kaspersky-labs.com/FD126C42-EBFA-4E12-B309-BB3FDD723AC1/main.js" charset="UTF-8"></script></head>
<body class="flipInX">
  <header class="header headerBk">
    <div class="container50">
        <div class="logo">
            <!-- <img class="logImg img-fluid" src="images/logo-blk.png" />  -->
            <a href="http://demo.knowledgesynonyms.com/ds-v2/"><img class="iconLogoImg img-fluid" src="images/favi-icon-blk.png" /> </a>
            <span class="pagetitle pageinnertitle">Digital  Activation</span>
        </div>
        <div class="menuIcon menuIconBlk" onclick="myFunction(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            Menu
        </div>
        <div class="closeIcon hover-target">
          <a href="http://demo.knowledgesynonyms.com/ds-v2/#4">
            <img class="img-fluid" src="images/close-icon.png" />
          </a>
        </div>
    </div>
  </header>
  <div class="sidebar">
    <div class="menuIcon menuIconClose hover-target" >
        <div class="bar1"></div>
        <div class="bar2"></div>
        Close
    </div>
    <nav id="menu"> 
      <ul class="parent-menu hover-target "> 
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#1">What we do <span>We do good</span></a>
        </li>
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#2"  >Branding<span>Identity Creation/ Web/ Content Strategy/ SEO and more</span></a>
        </li>
        <li>
          <a  href="http://demo.knowledgesynonyms.com/ds-v2/#3">Digital Experience <span>Campaign Strategy/ Interactive Marketing/ Social Media and more</span></a>          
        </li>
        </li>
        <li><a data-index="4" href="http://demo.knowledgesynonyms.com/ds-v2/#4">Digital Activation <span>Ecommerce platforms/ AR/VR/ Gamification/ Hybrid analytics and more</span></a></li>
        <li><a data-index="5" href="http://demo.knowledgesynonyms.com/ds-v2/#5">Who we are <span>The good folks</span></a></li>
        <li><a data-index="6" href="http://demo.knowledgesynonyms.com/ds-v2/#6">Goodies <span>Some extra input</span></a></li>
        <li><a data-index="7" href="http://demo.knowledgesynonyms.com/ds-v2/#7">Get in touch <span>With a cup of coffee</span></a></li>
        
      </ul> 
    </nav>
    <div class="sideFooter">
      <ul class="sideSocial">
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/facebook-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/linkedin-g.png"></a></li>
          <li><a href="" class="hover-target"><img class="img-fluid" src="images/twitter-g.png"></a></li>
      </ul>
      <a class="notdisplaymb hover-target" href="mailto:info@knowledgesynonyms.com">info@knowledgesynonyms.com</a>
    </div>
  </div>
<main>
  <div data-scroll class="page page--layout-2 transformBox">
    <!-- <div class="wrapper-main slide-in-right">
      <div class="container50">
        <div class="dctivationsection">
            <div class="about1 flexContainer heightVH">
                <div class="flex60 tabletDisplay MB">
                    <img class="img-fluid" src="images/digital_activation2.jpg">
                </div>
                <div class="flex40 about1Left textBoxLeft in-left">
                    <div class="textinnerbox">
                        <h1 class="textColored hover-target text-what-top"><span class="Display-MB">Digital Activation</span>Back to the future</h1>
                        <div class="textBoxLeftInner">
                          <h4 class="subheadeing">01 Ecommerce platforms</h4>
                          <p >Sell in the market where the customers spend most of their time in today’s small screen obsessed world, there is a rising need for establishing a brand’s presence on ecommerce platforms. It is important to find engaging ways to attract customers to your ecommerce platform.</p>  
                        </div>
                    </div>
                </div>
                <div class="flex60 tabletNotDisplay in-right">
                    <img class="img-fluid desktop" src="images/digital_activation2.jpg">
                    <img class="img-fluid ipad-lanscap" src="images/DS_ipad_530x6107.jpg">
                </div>
            </div>
        </div>
      </div>
    </div> -->
    <div class="content content--full content--alternate">
      <div class="container50">
        <div class="row">
            <div class="flex40"></div>
            <div class="flex60 text-250-right in-right">
              <div class="textinnerbox">
                  <h4 class="subheadeing">02 Social / collaborative platform experience design</h4>
                  <p>In Progress</p>
              </div>
            </div>
        </div>

        <div class="row">
            <div class="flex40 text-250-left in-left">
                <h1 class="digital-act-heading">Hold it here,<br>swipe there,<br/>you know</h1>
            </div>
            <div class="flex60"></div>
            <div class="flex40"></div>
            <div class="flex60 text-250-right in-right">
                <div class="textinnerbox">
                    <h4 class="subheadeing">03 AR/VR</h4>
                    <p>Help your customers “see” how your product looks and functions in real life right on the screen in your hand with our AR/VR solutions. To ensure the sustainability of a brand, it is important to keep up with technology. The digital transformation roadmap of a company has to include investments in assets creation like digital assets, 3D Models, UI/UX experience etc. </p>
                </div>
            </div>
            <div class="flex40 text-250-left in-left">
                <div class="textinnerbox">
                    <h4 class="subheadeing">04 Gamification</h4>
                    <p>What you hear or read needs to be repeated, what you experience stays with you. Let your users remember your product by “living the experience” with our gamification solutions. Our Gamers’  team collaborates with you to create some knock out games to personalize the customer experience.</p>
                </div>
            </div>
            <div class="flex60"></div>
            <div class="flex40"></div>
            <div class="flex60 text-250-right in-right">
              <div class="content__item ">	
                <div class="imgbox" ><img class="img-fluid content__item-img" src="images/silly-little.jpg"></div>
              </div>              
            </div>
        </div>

        <div class="row">
            <div class="flex40 text-250-left in-left">
                <h1 class="digital-act-heading">Silly little<br>things</h1>
            </div>
            <div class="flex60"></div>
        </div>
        <div class="row">
            <div class="flex40"></div>
            <div class="flex60 text-250-right in-right">
                <img class="img-fluid Sillylittle" src="images/gettyimages.jpg">
            </div>
        </div>
        <div class="row">
          <div class="flex40 text-250-left in-left">  
              <div class="textinnerbox">                  
                  <h4 class="subheadeing">05  Product prototypes</h4>
                  <p >Are you on the road of creating a product. At whatever stage of product prototyping you are-Visual, Proof-of-concept, or presentation, we can bring your idea to life. We walk through the entire process with you, from prototyping to the final product. </p>
              </div>
          </div>
          <div class="flex60"></div>
        </div>
        <div class="row">
            <div class="flex40"></div>
            <div class="flex60 text-250-right in-right">     
              <div class="textinnerbox">                     
                  <h4 class="subheadeing">06 Hybrid analytics</h4>
                  <p >We are talking action and data. Our engineering squad skillfully provides best-in-class analytics by blending high-speed innovation from the cloud with trusted on-premise competencies. This integrated hybrid solution expands analytic insights without sacrificing your existing on-premise investments.<br/>Rest assured your company will be able to provide a seamless experience with any consistent, modern user with shared functionality between on-premise and cloud deployments.</p>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="flex40 text-250-left in-left">
                <img class="img-fluid" src="images/hybrid.jpg">
            </div>
            <div class="flex60"></div>              
            <div class="col-md-12">
                <div class="nextTopic hover-target" ><a href="http://demo.knowledgesynonyms.com/ds-v2/#5">Who We Are<img class="img-fluid arrow-blink" src="images/next-icon.png"></a></div>
            </div>                    
        </div>                
      </div>
    </div>
  </div>
</main>


<?php include 'footer.php' ?>